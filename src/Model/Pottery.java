package Model;

import java.util.Date;

public class Pottery extends Product {

    private String company;

    public Pottery(String productID, String name, double price, int amount,
                   Date inputDay, String company) {
        super(productID, name, price, amount, inputDay);
        this.company = company;
        setVAT(0.1);
    }

    @Override
    public void setVAT(double VAT) {
        super.setVAT(0.1);
    }

    @Override
    public void setStatus(String status) {
        if(getAmount() > 50 && getInventoryDay() > 10 ){
            super.setStatus("Bán Chậm");
        }else {
            super.setStatus("Bình thường");
        }
    }

    @Override
    public void display() {
        super.display();
        System.out.println("Nhà sản xuất: " + company);
        System.out.println("Tình trạng bán buôn: " + super.getStatus());
    }

}
