package Model;

import Service.BillDate;

import java.util.Date;

public class Product {
    private String productID;
    private String name;
    private double price;
    private int amount;
    private Date inputDay;
    private long inventoryDay;
    private String Status;
    private double VAT;

    public Product(String productID, String name, double price, int amount, Date inputDay) {
            this.productID = productID;
            this.name = name;
            this.price = price;
            this.amount = amount;
            this.inputDay = inputDay;
    }

    public String getProductID(){
        return productID;
    }

    public String getName(){
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public Date getInputDay() {
        return inputDay;
    }

    public long getInventoryDay() {
        return inventoryDay;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getStatus() {
        return Status;
    }

    public void setVAT(double VAT) {
        this.VAT = price*VAT;
    }

    public void display() {
        System.out.println("Mã hàng hóa: " + productID);
        System.out.println("Tên hàng hóa: " + name);
        System.out.println("Giá bán: " + price);
        System.out.println("Thuế VAT: " + VAT);
        System.out.println("Số lượng: " + amount);
        System.out.println("Ngày nhập kho: " + BillDate.PrintDate(inputDay));
    }

}
