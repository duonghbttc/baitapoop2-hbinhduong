package Model;

import Service.BillDate;

import java.util.Date;

public class Food extends Product {

    private Date manufacturingDate;
    private Date expiryDate;

    public Food(String productID, String name, double price, int amount,
                Date inputDay, Date manufacturingDate, Date expiryDate) {
        super(productID, name, price, amount, inputDay);
        this.manufacturingDate = manufacturingDate;
        this.expiryDate = expiryDate;
        setVAT(0.05);
    }

    @Override
    public void setVAT(double VAT) {
        super.setVAT(0.05);
    }

    @Override
    public void setStatus(String status) {
        if(BillDate.CalculatorDate(expiryDate) < 0 && getInventoryDay() > 0 ){
            super.setStatus("Khó bán");
        }else {
            super.setStatus("Bình thường");
        }
    }

    @Override
    public void display() {
        super.display();
        System.out.println("Ngày sản xuất: " + manufacturingDate);
        System.out.println("Ngày hết hạn: " + expiryDate);
        System.out.println("Tình trạng bán buôn: " + super.getStatus());
    }

}
