package Model;

import java.util.Date;

public class Electronic extends Product {

    private int warrantyPeriod;
    private int wattage;


    public Electronic(String productID, String name, double price, int amount,
                      Date inputDay, int warrantyPeriod, int wattage) {
        super(productID, name, price, amount, inputDay);
        this.warrantyPeriod = warrantyPeriod;
        this.wattage = wattage;
        setVAT(0.1);
    }

    @Override
    public String getProductID() {
        return super.getProductID();
    }


    @Override
    public void setStatus(String status) {
        if(getAmount() < 3){
            status = "Bán được";
            super.setStatus(status);
        }else {
            status = "Bình thường";
            super.setStatus(status);
        }
    }

    @Override
    public void display() {
        super.display();
        System.out.println("Thời gian bảo hành: " + warrantyPeriod + " tháng");
        System.out.println("Công suất máy: " + wattage + "KW");
        System.out.println("Tình trạng bán buôn: " + super.getStatus());
    }


}
