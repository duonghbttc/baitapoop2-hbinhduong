import Service.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ElectronicService electronicService = new ElectronicService();
        PotteryService potteryService = new PotteryService();
        FoodService foodService = new FoodService();

        Scanner sc = new Scanner(System.in);
        boolean exit = false;
        int luachon = 0;
        showMenu();
        while (true) {
            luachon = sc.nextInt();
            switch (luachon) {
                case 1:
                    int DM = 0;
                    System.out.println("1. Thêm sản phẩm điện máy ");
                    System.out.println("2. Xem toàn bộ các sản phẩm ");
                    DM = sc.nextInt();
                    switch (DM){
                        case 1:
                            electronicService.addProduct();
                            break;
                        case 2:
                            electronicService.showProduct();
                            break;
                    }
                    break;
                case 2:
                    int TP = 0;
                    System.out.println("1. Thêm sản phẩm thực phẩm ");
                    System.out.println("2. Xem toàn bộ các sản phẩm ");
                    TP = sc.nextInt();
                    switch (TP){
                        case 1:
                            foodService.addProduct();
                            break;
                        case 2:
                            foodService.showProduct();
                            break;
                    }
                    break;
                case 3:
                    int SS = 0;
                    System.out.println("1. Thêm sản phẩm thực phẩm ");
                    System.out.println("2. Xem toàn bộ các sản phẩm ");
                    SS = sc.nextInt();
                    switch (SS){
                        case 1:
                            potteryService.addProduct();
                            break;
                        case 2:
                            potteryService.showProduct();
                            break;
                    }
                    break;
                default:
                    System.out.println("invalid! please choose action in below menu:");
                    break;
            }
            if (exit) {
                break;
            }
            showMenu();
        }
    }

    public static void showMenu() {
        System.out.println("");
        System.out.println("1. Hàng Điện Máy. ");
        System.out.println("2. Hàng Thực Phẩm. ");
        System.out.println("3. Hàng Sành Sứ. ");
        System.out.println("<========================================================> \n");
    }
}

