package Service;

import Model.Product;
import Model.Food;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class FoodService extends ProductService {

    ArrayList<Food> foods = new ArrayList<>();

    public Food addProduct() {

        Scanner sc = new Scanner(System.in);
        Product product = super.addProduct();

        System.out.println("Nhập ngày sản xuất(dd/MM/yyyy): ");
        String beginDate = sc.next();
        Date manufacturingDate = BillDate.SaveDate(beginDate);
        System.out.println("Nhập ngày hết hạn(dd/MM/yyyy): ");
        String endDate = sc.next();
        Date expiryDate = BillDate.SaveDate(endDate);

        if(BillDate.ComparisonDate(manufacturingDate, expiryDate) < 0){
            System.out.println("Sai ngày sản xuất, vui lòng nhập lại: ");
            addProduct();
        }

        Food food = new Food(product.getProductID(), product.getName(), product.getPrice(),
                             product.getAmount(), product.getInputDay(), manufacturingDate, expiryDate );
        String status = "";
        food.setStatus(status);
        checkID(foods, food);
        return food;
    }

    @Override
    public void checkID(ArrayList<Food> arrayList, Food food) {

        boolean check = true;
        for (int i = 0; i < arrayList.size(); i++){
            if(arrayList.get(i).getProductID().equals(food.getProductID())){
                check = false;
                break;
            };
        }
        if(check == true){
            arrayList.add(food);
        }else{
            System.out.println("***************************************************");
            System.out.println("Mã hàng trùng, vui lòng nhập lại mã hàng!!!");
            System.out.println("***************************************************");
            System.out.println("");
            addProduct();
        }
    }

    @Override
    public void showProduct() {

        for (int i = 0; i < foods.size(); i++){
            foods.get(i).display();
        }
    }
}
