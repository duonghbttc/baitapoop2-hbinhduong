package Service;

import Model.Product;
import Model.Pottery;

import java.util.ArrayList;
import java.util.Scanner;

public class PotteryService extends ProductService {

    ArrayList<Pottery> potteries = new ArrayList<>();

    public Pottery addProduct() {

        Scanner sc = new Scanner(System.in);
        Product product = super.addProduct();

        System.out.println("Nhập tên nhà sản xuất: ");
        String company = sc.next();

        Pottery pottery = new Pottery(product.getProductID(), product.getName(), product.getPrice(),
                                      product.getAmount(), product.getInputDay(), company );
        String status = "";
        pottery.setStatus(status);
        checkID(potteries, pottery);
        return pottery;
    }

    @Override
    public void checkID(ArrayList<Pottery> arrayList, Pottery pottery) {

        boolean check = true;
        for (int i = 0; i < arrayList.size(); i++){
            if(arrayList.get(i).getProductID().equals(pottery.getProductID())){
                check = false;
                break;
            };
        }
        if(check == true){
            arrayList.add(pottery);
        }else{
            System.out.println("***************************************************");
            System.out.println("Mã hàng trùng, vui lòng nhập lại mã hàng!!!");
            System.out.println("***************************************************");
            System.out.println("");
            addProduct();
        }
    }

    @Override
    public void showProduct() {
        for (int i = 0; i < potteries.size(); i++){
            potteries.get(i).display();
        }
    }
}
