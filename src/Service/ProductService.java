package Service;

import IService.IService;
import Model.Electronic;

import Model.Product;
import Model.Pottery;
import Model.Food;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class ProductService implements IService {

    @Override
    public Product addProduct() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mã hàng hóa: ");
        String productID = sc.nextLine();
        if (productID == null | productID.length() == 0) {
            System.out.println("Tên hàng hóa trống, vui lòng nhập lại");
            addProduct();
        }

        System.out.println("Nhập tên hàng hóa: ");
        String name = sc.nextLine();
        if (name == null | name.length() == 0) {
            System.out.println("Tên hàng hóa trống, vui lòng nhập lại");
            addProduct();
        }

        System.out.println("Nhập số lượng: ");
        int amount = sc.nextInt();
        if (amount < 0) {
            System.out.println("Số lượng sai, vui lòng nhập lại");
            addProduct();
        }

        System.out.println("Nhập đơn giá: ");
        double price = sc.nextDouble();
        if (price <= 0) {
            System.out.println("Sai giá cả, vui lòng nhập lại");
            addProduct();
        }

        System.out.println("Nhập ngày nhập kho(dd/MM/yyyy): ");
        String day = sc.next();
        Date inputDay = BillDate.SaveDate(day);


        Product product = new Product(productID, name, price, amount, inputDay);
        return product;
    }

    @Override
    public void showProduct() {

    }

    public void checkID(ArrayList<Electronic> electronics, Electronic electronic) {


    }

    public void checkID(ArrayList<Pottery> potteries, Pottery pottery) {


    }

    public void checkID(ArrayList<Food> foods, Food food) {


    }
}
