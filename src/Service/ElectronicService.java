package Service;

import Model.Electronic;
import Model.Product;

import java.util.ArrayList;
import java.util.Scanner;

public class ElectronicService extends ProductService {

    ArrayList<Electronic> electronics = new ArrayList<Electronic>();

    public Electronic addProduct() {

        Scanner sc = new Scanner(System.in);
        Product product = super.addProduct();

        System.out.println("Nhập thời gian bảo hành(tháng): ");
        int warrantyPeriod = sc.nextInt();
        if(warrantyPeriod < 0){
            System.out.println("Số lượng sai, vui lòng nhập lại");
            addProduct();
        }

        System.out.println("Nhập công suất(KW): ");
        int wattage = sc.nextInt();
        if(wattage <= 0){
            System.out.println("Công suất sai, vui lòng nhập lại");
            addProduct();
        }

        Electronic electronic = new Electronic(product.getProductID(), product.getName(), product.getPrice(),
                                               product.getAmount(), product.getInputDay(), warrantyPeriod, wattage);
        String status = "";
        electronic.setStatus(status);
        checkID(electronics, electronic);
        return electronic;
    }

    @Override
    public void checkID(ArrayList<Electronic> arrayList, Electronic electronic) {

        boolean check = true;
        for (int i = 0; i < arrayList.size(); i++){
            if(arrayList.get(i).getProductID().equals(electronic.getProductID())){
                check = false;
                break;
            };
        }
        if(check == true){
            arrayList.add(electronic);
        }else{
            System.out.println("***************************************************");
            System.out.println("Mã hàng trùng, vui lòng nhập lại mã hàng!!!");
            System.out.println("***************************************************");
            System.out.println("");
            addProduct();
        }

    }

    @Override
    public void showProduct() {
        for (int i = 0; i < electronics.size(); i++){
            electronics.get(i).display();
        }
    }
}
