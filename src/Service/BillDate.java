package Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BillDate {

    public static Date SaveDate(String inputStringDate) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = formatter.parse(inputStringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String PrintDate(Date inputDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateFormat = formatter.format(inputDate);
        return dateFormat;
    }

    public static long CalculatorDate(Date inputDate) {
        String dateStart = PrintDate(inputDate);
        java.util.Date date = new java.util.Date();
        String dateStop = PrintDate(date);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        long diffDays = 0;
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            long diff = d2.getTime() - d1.getTime();
            diffDays =  diff / (24 * 60 * 60 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return diffDays;
    }

    public static long ComparisonDate(Date dateA, Date dateB) {
        String dateStart = PrintDate(dateA);
        String dateStop = PrintDate(dateB);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        long diffDays = 0;
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            long diff = d2.getTime() - d1.getTime();
            diffDays =  diff / (24 * 60 * 60 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return diffDays;
    }
}



